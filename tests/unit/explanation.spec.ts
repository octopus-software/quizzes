import {shallowMount} from '@vue/test-utils'
import Explanation from '@/components/explanation.vue'

describe('explanation.vue', () => {
    const shallowMountWithExplanation = (explanation: String) => {
        return  shallowMount(Explanation, {
            propsData: {
                explanation: explanation
            }
        });
    };

    const explanation = 'テスト解説です';
    it('Explanation message is equal to that props', () => {
        const expectedMessage = '解説 テスト解説です';
        expect(shallowMountWithExplanation(explanation).text()).toBe(expectedMessage);
    });
});
