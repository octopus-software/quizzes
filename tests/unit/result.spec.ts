import {shallowMount} from '@vue/test-utils'
import Result from '@/components/result.vue'

describe('result.vue', () => {
    const result = 'test';
    const wrapper = shallowMount(Result, {
        propsData: {
            result: result
        }
    });
    it('Result message is equal to that props', () => {
        expect(wrapper.props().result).toBe('test');
    });
});
