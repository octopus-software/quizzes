import {shallowMount} from '@vue/test-utils'
import Score from '@/components/score.vue'

describe('score.vue', () => {
    const shallowMountWithScore = (score: number) => {
        return  shallowMount(Score, {
            propsData: {
                score: score
            }
        });
    };

    it('Score message is equal to that props', () => {
        expect(shallowMountWithScore(30).props().score).toBe(30);
    });

    it('renders the correct markup score 30', () => {
        const expectedMessage = 'あなたの得点率は30%です！ 今後に期待！';
        expect(shallowMountWithScore(30).text()).toBe(expectedMessage)
    });

    it('renders the correct markup score 45', () => {
        const expectedMessage = 'あなたの得点率は45%です！ あともう少し！';
        expect(shallowMountWithScore(45).text()).toBe(expectedMessage)
    });

    it('renders the correct markup score 65', () => {
        const expectedMessage = 'あなたの得点率は65%です！ なかなかですね！';
        expect(shallowMountWithScore(65).text()).toBe(expectedMessage)
    });

    it('renders the correct markup score 85', () => {
        const expectedMessage = 'あなたの得点率は85%です！ 十分な知識量です！';
        expect(shallowMountWithScore(85).text()).toBe(expectedMessage)
    });

    it('renders the correct markup score 100', () => {
        const expectedMessage = 'あなたの得点率は100%です！ 満点です！素晴らしい！';
        expect(shallowMountWithScore(100).text()).toBe(expectedMessage)
    });
});
