import {shallowMount} from '@vue/test-utils'
import Question from '@/components/question.vue'

describe('question.vue', () => {
    const shallowMountWithParameters = (questionIndex: number, questionText: String) => {
        return  shallowMount(Question, {
            propsData: {
                questionIndex: questionIndex,
                questionText: questionText
            }
        });
    };
    const index = 1;
    const text = 'テスト問題です';
    console.log(shallowMountWithParameters(index, text).text());

    it('Question message is equal to that props', () => {
        const expectedMessage = 'Question.1 テスト問題です';
        expect(shallowMountWithParameters(index, text).text()).toBe(expectedMessage);
    });
});
